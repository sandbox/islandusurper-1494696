<?php

/**
 * @file
 * Support for order destinations.
 */

class MigrateDestinationUcOrder extends MigrateDestinationEntity {

  static public function getKeySchema() {
    return array(
      'order_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'ID of destination order',
      ),
    );
  }

  public function __construct(array $options = array()) {
    parent::__construct('uc_order', 'uc_order', $options);
  }

  public function fields() {
    $fields = array();

    $fields['order_id'] = t('Order: Existing order ID');
    $fields['uid'] = t('Order: Customer (uid)');
    $fields['order_status'] = t('Order: Status');
    $fields['order_total'] = t('Order: Total');
    $fields['product_count'] = t('Order: Product count');
    $fields['primary_email'] = t('Order: Primary email');
    $fields['delivery_first_name'] = t('Order: Delivery address first name');
    $fields['delivery_last_name'] = t('Order: Delivery address last name');
    $fields['delivery_phone'] = t('Order: Delivery address phone number');
    $fields['delivery_company'] = t('Order: Delivery address company name');
    $fields['delivery_street1'] = t('Order: Delivery address street');
    $fields['delivery_street2'] = t('Order: Delivery address street (line 2)');
    $fields['delivery_city'] = t('Order: Delivery address city');
    $fields['delivery_zone'] = t('Order: Delivery address state/zone/province');
    $fields['delivery_postal_code'] = t('Order: Delivery address postal code');
    $fields['delivery_country'] = t('Order: Delivery address country');
    $fields['billing_first_name'] = t('Order: Billing address first name');
    $fields['billing_last_name'] = t('Order: Delivery address last name');
    $fields['billing_phone'] = t('Order: Delivery address phone number');
    $fields['billing_company'] = t('Order: Delivery address company name');
    $fields['billing_street1'] = t('Order: Delivery address street');
    $fields['billing_street2'] = t('Order: Delivery address street (line 2)');
    $fields['billing_city'] = t('Order: Delivery address city');
    $fields['billing_zone'] = t('Order: Delivery address state/zone/province');
    $fields['billing_postal_code'] = t('Order: Delivery address postal code');
    $fields['billing_country'] = t('Order: Delivery address country');
    $fields['payment_method'] = t('Order: Payment method');
    $fields['data'] = t('Order: Serialized data');
    $fields['created'] = t('Order: Created timestamp');
    $fields['modified'] = t('Order: Modified timestamp');
    $fields['host'] = t('Order: IP address of customer');
    $fields['currency'] = t('Order: Currency paid');

    // Add in anything provided by handlers
    $fields += migrate_handler_invoke_all('Entity', 'fields', $this->entityType, $this->bundle);
    $fields += migrate_handler_invoke_all('UcOrder', 'fields');

    return $fields;
  }

  /**
   * Delete an order.
   *
   * @param array $key
   */
  public function rollback(array $order_id) {
    migrate_instrument_start('uc_order_delete');
    uc_order_delete(reset($order_id));
    migrate_instrument_stop('uc_order_delete');
  }

  /**
   * Import a single order.
   *
   * @param $order
   *  Order object to build. Prefilled with any fields mapped in the Migration.
   * @param $row
   *  Raw source data object - passed through to prepare/complete handlers.
   * @return array
   *  Array of key fields (order_id only in this case) of the node that was
   *  saved if successful. FALSE on failure.
   */
  public function import(stdClass $order, stdClass $row) {
    // Updating previously-migrated content?
    $migration = Migration::currentMigration();
    if (isset($row->migrate_map_destid1)) {
      // Make sure is_new is off
      $order->is_new = FALSE;
      if (isset($order->order_id)) {
        if ($order->order_id != $row->migrate_map_destid1) {
          throw new MigrateException(t("Incoming order_id !order_id and map destination order_id !destid1 don't match",
            array('!order_id' => $order->order_id, '!destid1' => $row->migrate_map_destid1)));
        }
      }
      else {
        $order->order_id = $row->migrate_map_destid1;
      }
    }
    if ($migration->getSystemOfRecord() == Migration::DESTINATION) {
      if (!isset($order->order_id)) {
        throw new MigrateException(t('System-of-record is DESTINATION, but no destination order_id provided'));
      }
      $old_order = uc_order_load($order->order_id);
      if (!isset($order->created)) {
        $order->created = $old_order->created;
      }
      if (!isset($order->uid)) {
        $order->uid = $old_order->uid;
      }
    }

    // Invoke migration prepare handlers
    $this->prepare($order, $row);

    // Trying to update an existing order
    if ($migration->getSystemOfRecord() == Migration::DESTINATION) {
      // Incoming data overrides existing data, so only copy non-existent fields
      foreach ($old_order as $field => $value) {
        // An explicit NULL in the source data means to wipe to old value (i.e.,
        // don't copy it over from $old_node)
        if (property_exists($order, $field) && $order->$field === NULL) {
          // Ignore this field
        }
        elseif (!isset($order->$field)) {
          $order->$field = $old_order->$field;
        }
      }
    }

    if (isset($order->order_id) && !(isset($order->is_new) && $order->is_new)) {
      $updating = TRUE;
    }
    else {
      $updating = FALSE;
      // Unfortunately, uc_order_new() doesn't allow us to specify an order_id.
      if (isset($order->order_id)) {
        migrate_instrument_start('uc_order_new');
        drupal_write_record('uc_orders', $order);
        uc_order_module_invoke('new', $order, NULL);
        migrate_instrument_stop('uc_order_new');
      }
      else {
        migrate_instrument_start('uc_order_new');
        $new_order = uc_order_new($order->uid);
        migrate_instrument_stop('uc_order_new');
        $order->order_id = $new_order->order_id;
      }
    }

    migrate_instrument_start('uc_order_save');
    uc_order_save($order);
    migrate_instrument_stop('uc_order_save');

    if (isset($order->order_id)) {
      if ($updating) {
        $this->numUpdated++;
      }
      else {
        $this->numCreated++;
      }

      $return = array($order->order_id);
    }
    else {
      $return = FALSE;
    }

    $this->complete($order, $row);
    return $return;
  }

}

class MigrateUcOrderProductHandler extends MigrateDestinationHandler {

  public function __construct() {
    $this->registerTypes(array('UcOrder'));
  }

  public function fields() {
    return array(
      'products' => t('Order: Products'),
    );
  }

  public function prepare($entity, stdClass $row) {
    // At the base level, a product is just a nid and a quantity. If that's all
    // we are given, the rest can be determined from the node. However if more
    // is given, then we can just use that the way it is.
    if (!isset($row->products) || !is_array($row->products)) {
      return FALSE;
    }

    foreach ($row->products as &$product) {
      if (!isset($product->nid)) {
        $product->nid = 0;
      }
      if (!isset($product->qty)) {
        $product->qty = 1;
      }

      // Ugh. Should we use node_load() here?
    }
  }

}
