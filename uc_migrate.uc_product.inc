<?php

class MigrateUcProductHandler extends MigrateDestinationHandler {

  function __construct() {
    $this->registerTypes(array('node'));
  }

  function fields($entity_type, $bundle) {
    $fields = array();

    if (!module_exists('uc_product')) {
      return;
    }

    if (!uc_product_is_product($bundle)) {
      return;
    }

    $fields += array(
      'model' => t('Product: SKU or model number.'),
      'list_price' => t('Product: Suggested retail price.'),
      'cost' => t('Product: The amount the store pays to sell the product.'),
      'sell_price' => t('Product: The amount the customer pays for the product.'),
      'weight' => t('Product: Physical weight.'),
      'weight_units' => t('Product: Unit of measure for the weight field.'),
      'length' => t('Product: Physical length of the product or its packaging.'),
      'width' => t('Product: Physical width of the product or its packaging.'),
      'height' => t('Product: Physical height of the product or its packaging.'),
      'length_units' => t('Product: Unit of measure for the length, width, and height.'),
      'pkg_qty' => t('Product: The number of this product that fit in one package.'),
      'default_qty' => t('Product: The default value for the quantity field in the "Add to Cart" form.'),
      'unique_hash' => t('Product: Hash of initial values.'),
      'ordering' => t('Product: The sort criteria for products.'),
      'shippable' => t('Product: Boolean flag signifying that the product can be shipped.'),
    );

    if (module_exists('uc_quote')) {
      $fields += array(
        'shipping_type' => t('Product: Type of shipping package.'),
        'shipping_address' => t('Product: Default shipping origination address.'),
      );
    }

    if (module_exists('uc_flatrate')) {
      $fields['flatrate'] = t('Product: Flat shipping rate overrides.');
    }

    if (module_exists('uc_ups')) {
      $fields['ups'] = t('Product: UPS package type.');
    }

    if (module_exists('uc_weightquote')) {
      $fields['weightquote'] = t('Product: Weight-based shipping rate overrides.');
    }

    return $fields;
  }

}
